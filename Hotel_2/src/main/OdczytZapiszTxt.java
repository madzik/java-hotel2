package main;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.*;

/**
 * Created by Magda on 2017-10-20.
 */
public class OdczytZapiszTxt {
    public static void main(String[] args){

       File plik = new File( "plik.txt");
OdczytZapiszTxt z = new OdczytZapiszTxt();

        System.out.println("Zapis");
        try {
            z.zapisPlikuTxt(plik);
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("Odczyt");
        z.odczytPlikuTxt(plik);

    }

    /**
     * Zapis
     * @param nazwaPliku
     */
    public void zapisPlikuTxt(File nazwaPliku) throws IOException {

        //File plikDoZapisu = new File(nazwaPliku);
        BufferedWriter zapis = null;
        try {
            zapis = new BufferedWriter(new FileWriter(nazwaPliku));

            //flush czyszczenie przed zapisem
            zapis.flush();

            zapis.write("Magda Kow 12321345769\n");
            zapis.write("Ola Zbif 1234455523\n");
            zapis.write("Maniusiowo Zygfryt 1234567873\n");
            zapis.write("Anna Zabłosck 12321345769\n");
            zapis.write("Sam Dom 12321345765\n");
            zapis.write("Dom Sam 12321345766\n");
            zapis.write("Zab Sam 12321345764\n");
            zapis.write("Zuzanna Zabłek 12321345761");

            zapis.close();
        } finally {
            zapis.close();
        }

    }

    /**
     * Odczyt
     * @param nazwaPliku
     */

    public void odczytPlikuTxt(File nazwaPliku){

        //File plikDoZapisu = new File(nazwaPliku);


        try{
            BufferedReader odczyt = new BufferedReader(new FileReader(nazwaPliku));
            String wiersz = null;

            String[] w = null;
            while((wiersz = odczyt.readLine()) != null){
                w = wiersz.split("/");
            }
            odczyt.close();

            for(int x=0; x<w.length; x++){
                System.out.println(w[x]);
            }

        }catch(IOException e){
            e.printStackTrace();

        }

        }
}
